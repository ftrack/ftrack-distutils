import os
import sys
from distutils.core import Command
import fileinput
from distutils.dir_util import copy_tree, remove_tree, mkpath
import subprocess
from distutils.spawn import find_executable


class build_resource(Command):
    description = "ftrack connect resources"

    user_options = [
        ('replace-qt-imports=', None, 'specify whether to replace PySide* imports with generic Qt module'),
        ('resource-path=', None , 'Specify plugin resource path'),
        ('themes=', None , 'if themes are passed (comma separated), sass will try to build from <resource-path>/scss before building resource qrc'),
        ('target-path', None, 'specify the output of the final compiled resource path')
    ]

    def initialize_options(self):
        self.target_path=None
        self.replace_qt_imports = True
        self.pyside_rcc_command = 'pyside2-rcc'
        self.resource_path = None
        self.themes = []

    def finalize_options(self):
        assert self.target_path != None

        self.build_lib = os.path.abspath(self.get_finalized_command('build').build_lib)
        self.build_dir = os.path.dirname(self.build_lib)

        self.resource_path = os.path.abspath(self.resource_path or 'resource')
        self.sass_path = os.path.join(self.resource_path, 'sass')
        self.css_path = self.resource_path
        self.themes = self.themes.split(',')
        self.resource_source_path = os.path.join(self.resource_path, 'resource.qrc')

    def replace_imports(self):
        replace = r'from Qt import QtCore'
        for line in fileinput.input(self.target_path, inplace=True, mode='r'):
            if r'import QtCore' in line:
                # Calling print will yield a new line in the resource file.
                sys.stdout.write(line.replace(line, replace))
            else:
                sys.stdout.write(line)

    def run_build_qt_resource(self):
        executable = find_executable(self.pyside_rcc_command)

        if not executable:
            raise IOError('Not executable found for pyside2-rcc ')

        # Use the first occurrence if more than one is found.
        cmd = [
            executable,
            '-o',
            self.target_path,
            self.resource_source_path
        ]
        print('running : {}'.format(cmd))
        subprocess.check_call(cmd)

    def run_build_scss(self):
        print('Attempting to build scss....')

        try:
            import scss
        except ImportError:
            raise RuntimeError(
                'Error compiling sass files. Could not import "scss". '
                'Check you have the pyScss Python package installed.'
            )

        compiler = scss.Scss(
            search_paths=[self.sass_path]
        )
        for theme in self.themes:
            scss_source = os.path.join(self.sass_path, '{}.scss'.format(theme))
            css_target = os.path.join(self.css_path, '{}.css'.format(theme))
            compiled = compiler.compile(
                scss_file=scss_source
            )
            with open(css_target, 'w') as file_handle:
                file_handle.write(compiled)
                print('Compiled {0}'.format(css_target))

    def run_build_plugin(self):
        cmd_obj = self.distribution.get_command_obj('build_plugin')
        cmd_obj.resource_path = self.resource_path
        self.run_command('build_plugin')

    def run(self):
        if self.themes:
            print('Biling scss')
            self.run_build_scss()

        # build qt resources
        try:
            print('Building qt resource')
            self.run_build_qt_resource()
        except (subprocess.CalledProcessError, OSError):
            raise RuntimeError(
                'Error compiling resource.py using pyside-rcc. Possibly '
                'pyside-rcc could not be found. You might need to manually add '
                'it to your PATH. See README for more information.'
            )

        if self.replace_qt_imports:
            print('Replacing Qt Imports')
            self.replace_imports()

        self.run_build_plugin()



class build_plugin(Command):
    description = "ftrack connect plugin"

    user_options = [
        ('resource-path=', 'r', 'Specify plugin resource path'),
    ]

    def initialize_options(self):
        self.resource_path = None

    def finalize_options(self):
        self.resource_path = self.resource_path or 'resource'
        self.dist_dir = os.path.abspath(self.get_finalized_command('bdist').dist_dir)
        self.build_lib = os.path.abspath(self.get_finalized_command('build').build_lib)
        self.build_dir = os.path.dirname(self.build_lib)
        self.python_version = '{}{}'.format(*sys.version_info[0:2])

    def run(self):
        '''Run the build step.'''

        # cleanup
        if os.path.exists(self.build_dir):
            remove_tree(self.build_dir)

        if os.path.exists(self.dist_dir):
            remove_tree(self.dist_dir)

        mkpath(self.build_dir)
        mkpath(self.build_lib)

        # plugin variables
        addon_name = self.distribution.get_name()
        archive_name = '{}-v{}-py{}'.format(addon_name, self.distribution.get_version(), self.python_version)
        build_release_path = os.path.join(self.build_dir, archive_name)
        dist_release_path = os.path.join(self.dist_dir, archive_name)

        #plugin_paths
        staging_path = os.path.join(self.build_dir, archive_name)
        resource_path = os.path.abspath(self.resource_path)

        # get hook inside resources
        hook_path = os.path.join(resource_path, 'hook')
        
        # COPY HOOKS
        copy_tree(hook_path, os.path.join(staging_path, 'hook'))
        
        # create dependencies folder
        staging_build_path = os.path.join(staging_path, 'dependencies')

        mkpath(staging_build_path)
        # run pip install for dependencies
        subprocess.check_call(
            [sys.executable, '-m', 'pip', 'install','.','--target',staging_build_path]
        )
        
        self.make_archive(str(dist_release_path), 'zip', str(build_release_path))


__all__ = ('build_plugin', 'build_resource')
