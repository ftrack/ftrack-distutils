import os
from setuptools import setup, find_packages

ROOT_PATH = os.path.dirname(os.path.realpath(__file__))
SOURCE_PATH = os.path.join(ROOT_PATH, 'source')
README_PATH = os.path.join(ROOT_PATH, 'README.rst')

setup(
    name='test_plugin',
    version='0.0.1',
    description='test plugin build',
    long_description=open(README_PATH).read(),
    url='',
    package_dir={
        '': 'source'
    },
    author='Lorenzo Angeli',
    author_email='lorenzo.angeli@ftrack.com',
    license='Apache',
    keywords='ftrack connect plugin',
    packages=find_packages(SOURCE_PATH),
    setup_requires=[
        'ftrack.distutils'
    ],
    install_requires=[
        'ftrack-python-api'
    ]
)