import os
from setuptools import setup, find_packages

ROOT_PATH = os.path.dirname(os.path.realpath(__file__))
SOURCE_PATH = os.path.join(ROOT_PATH, 'source')
README_PATH = os.path.join(ROOT_PATH, 'README.rst')


version_template = '''
# :coding: utf-8
# :copyright: Copyright (c) 2021 ftrack

__version__ = {version!r}
'''


setup(
    name='ftrack.distutils',
    use_scm_version={
        'write_to': 'source/ftrack/_version.py',
        'write_to_template': version_template,
        'version_scheme': 'post-release'
    },
    description='ftrack distutils plugin',
    long_description=open(README_PATH).read(),
    url='',
    package_dir={
        '': 'source'
    },
    author='Lorenzo Angeli',
    author_email='lorenzo.angeli@ftrack.com',
    license='Apache',
    packages=find_packages(SOURCE_PATH),
    keywords='distutils ftrack connect',
    install_requires=[
        'pyScss >= 1.2.0, < 2',
    ],
    setup_requires=[
        'setuptools>=45.0.0',
        'setuptools_scm'
    ],
    entry_points={
      'distutils.commands': [
          'build_plugin=ftrack:build_plugin',
          'build_resource=ftrack:build_resource'
          ]
    }
)