ftrack distutils
################

Provides **bdist_ftrack_connect_plugin** custom command to distutils to build ftrack_integrations.


install
=======

install in edit mode with ::

    pip install -e .


include in requirements
=======================

Include the new module it in your *setup_requires* section of setup.py:

    setup_requires=[
        'ftrack.distutils'
    ]

configure
=========

.. note:

    This is still in wip to see what worth including as configuration

A setup.cfg has to be provided to pass some variables to the build system::

    [build_plugin]
    resource-path=./resource


or to build plugin with resource::

    [build_resource]
    resource-path=./resource
    themes=style
    target-path=source/test_plugin/ui/resource.


run 
===

once all is configured you can run to build just the plugin ::


    python setup.py build_plugin


or ::

    python setup.py build_resource

you should get the result in build folder
and a zip file in dist folder.


examples
========

Check the **examples folder** to see a fully functional setup.
install first this repo in edit mode and run the given command.






































inspired from
-------------

* https://github.com/charlesfleche/blender.distutils

